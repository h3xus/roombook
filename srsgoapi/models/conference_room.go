// models/conference_room.go
package models

import "time"

type ConferenceRoom struct {
	RoomID             int       `json:"room_id"`
	Name               string    `json:"name"`
	RoomDescription    string    `json:"room_description"`
	Delegation         bool      `json:"delegation"`
	Localization       int       `json:"localization"`
	RoomPriority       int       `json:"room_priority"`
	PermanentEquipment int       `json:"permanent_equipment"`
	MaxCapacity        int       `json:"max_capacity"`
	Availability       int       `json:"availability"`
	Session            int       `json:"session"`
	Catering           int       `json:"catering"`
	Created            time.Time `json:"created"`
	CreatedBy          int       `json:"created_by"`
	Updated            time.Time `json:"updated"`
	UpdatedBy          int       `json:"updated_by"`
}
