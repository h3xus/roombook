// models/booking.go
package models

import "time"

type Booking struct {
	ID               int       `json:"id"`
	TitleMeeting     string    `json:"title_meeting"`
	Participants     int       `json:"participants"`
	UserID           int       `json:"user_id"`
	RoomID           int       `json:"room_id"`
	Status           string    `json:"status"`
	BookingDateStart time.Time `json:"booking_date_start"`
	BookingDateEnd   time.Time `json:"booking_date_end"`
	CreatedAt        time.Time `json:"created_at"`
	BookingParams    int       `json:"booking_params"`
	Optionals        int       `json:"optionals"`
}
