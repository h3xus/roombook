// models/room_parameters.go
package models

import "time"

type RoomParameters struct {
	ID              int       `json:"id"`
	MaximumCapacity int       `json:"maximum_capacity"`
	Availability    int       `json:"availability"`
	Session         bool      `json:"session"`
	Setting         int       `json:"setting"`
	Tables          int       `json:"tables"`
	Cathering       int       `json:"cathering"`
	Created         time.Time `json:"created"`
	CreatedBy       int       `json:"created_by"`
	Updated         time.Time `json:"updated"`
	UpdatedBy       int       `json:"updated_by"`
}

// ... Add similar struct definitions for other tables ...
