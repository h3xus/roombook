// models/cr_localization.go
package models

type Localization struct {
	Loc_id   int    `json:"loc_id"`
	Building int    `json:building`
	Floor    int    `json:"floor"`
	Name     string `json:"name"`
}
