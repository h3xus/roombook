// main.go
package main

import (
	"log"
	"net/http"

	httpSwagger "github.com/swaggo/http-swagger" // Swagger middleware

	"srsgoapi/db"
	_ "srsgoapi/docs"
	"srsgoapi/handlers"
)

//	@title			Swagger Example API
//	@version		1.0
//	@description	This is a sample server celler server.
//	@termsOfService	http://swagger.io/terms/

//	@contact.name	API Support
//	@contact.url	http://www.swagger.io/support
//	@contact.email	support@swagger.io

//	@license.name	Apache 2.0
//	@license.url	http://www.apache.org/licenses/LICENSE-2.0.html

//	@host		localhost:8080
//	@BasePath	/api/v1

//	@securityDefinitions.basic	BasicAuth

//	@securityDefinitions.apikey	ApiKeyAuth
//	@in							header
//	@name						Authorization
//	@description				Description for what is this security definition being used

//	@securitydefinitions.oauth2.application	OAuth2Application
//	@tokenUrl								https://example.com/oauth/token
//	@scope.write							Grants write access
//	@scope.admin							Grants read and write access to administrative information

//	@securitydefinitions.oauth2.implicit	OAuth2Implicit
//	@authorizationUrl						https://example.com/oauth/authorize
//	@scope.write							Grants write access
//	@scope.admin							Grants read and write access to administrative information

//	@securitydefinitions.oauth2.password	OAuth2Password
//	@tokenUrl								https://example.com/oauth/token
//	@scope.read								Grants read access
//	@scope.write							Grants write access
//	@scope.admin							Grants read and write access to administrative information

// @securitydefinitions.oauth2.accessCode	OAuth2AccessCode
// @tokenUrl								https://example.com/oauth/token
// @authorizationUrl						https://example.com/oauth/authorize
// @scope.admin							Grants read and write access to administrative information
func main() {
	if err := db.InitDB(); err != nil {
		log.Fatalf("Error initializing database: %v", err)
	}
	// GetBookings lists all existing in db
	//
	//  @Summary      List bookings
	//  @Description  get bookings
	//  @Tags         bookings
	//  @Accept       json
	//  @Produce      json
	//  @Success      200  {array}   model.Account
	//  @Failure      400  {object}  httputil.HTTPError
	//  @Failure      404  {object}  httputil.HTTPError
	//  @Failure      500  {object}  httputil.HTTPError
	//  @Router       /bookings [get]
	http.HandleFunc("/bookings", handlers.GetBookingHandler)
	http.HandleFunc("/bookings/create", handlers.CreateBookingHandler)

	http.HandleFunc("/bookings/update", handlers.HealthCheckHandler)
	http.HandleFunc("/bookings/delete", handlers.HealthCheckHandler)
	http.HandleFunc("/bookings/user", handlers.HealthCheckHandler)
	http.HandleFunc("/bookings/week", handlers.HealthCheckHandler)

	http.HandleFunc("/rooms/available", handlers.HealthCheckHandler)
	http.HandleFunc("/rooms/availability", handlers.CheckAvailabilityHandler)

	http.HandleFunc("/rooms/locations/all", handlers.GetLocalizationsHandler)
	http.HandleFunc("/rooms/locations", handlers.GetRoomsByLocalizationHandler)
	http.HandleFunc("/rooms/all", handlers.GetRoomsHandler)
	// healthcheck
	http.HandleFunc("/health", handlers.HealthCheckHandler)

	// Swagger route
	http.Handle("/swagger/", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:7075/swagger/doc.json"), // The URL to access the JSON documentation.
	))

	log.Fatal(http.ListenAndServe(":7075", nil))
}
