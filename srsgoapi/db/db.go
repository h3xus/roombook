// db/db.go
package db

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"srsgoapi/models"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

// InitDB initializes the database connection.
func InitDB() error {
	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?parseTime=true", os.Getenv("DB_USERNAME"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_NAME"))
	var err error
	db, err = sql.Open("mysql", dataSourceName)
	if err != nil {
		return fmt.Errorf("error opening database: %v", err)
	}

	// Set the maximum number of open connections to the database.
	db.SetMaxOpenConns(10)

	// Set the maximum number of idle connections to the database.
	db.SetMaxIdleConns(5)

	// Set the maximum lifetime of a connection to the database.
	db.SetConnMaxLifetime(time.Minute * 5)

	err = db.Ping()
	if err != nil {
		return fmt.Errorf("error pinging database: %v", err)
	}

	log.Println("Database connection initialized successfully")
	return nil
}

// CloseDB closes the database connection.
func CloseDB() {
	if db != nil {
		err := db.Close()
		if err != nil {
			log.Printf("Error closing database: %v\n", err)
		}
	}
}

// GetDB returns the current database connection.
func GetDB() *sql.DB {
	return db
}

func GetBookings() ([]models.Booking, error) {
	rows, err := db.Query("SELECT * FROM booked_rooms")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var bookings []models.Booking
	for rows.Next() {
		var booking models.Booking
		var bookingDateStart, bookingDateEnd string

		err := rows.Scan(
			&booking.ID,
			&booking.TitleMeeting,
			&booking.Participants,
			&booking.UserID,
			&booking.RoomID,
			&booking.Status,
			&bookingDateStart, // Scan as string
			&bookingDateEnd,   // Scan as string
			&booking.CreatedAt,
			&booking.BookingParams,
			&booking.Optionals,
		)
		if err != nil {
			return nil, err
		}

		// Convert string to time.Time
		booking.BookingDateStart, err = time.Parse("2006-01-02T15:04:05Z07:00", bookingDateStart)
		if err != nil {
			return nil, err
		}

		booking.BookingDateEnd, err = time.Parse("2006-01-02T15:04:05Z07:00", bookingDateEnd)
		if err != nil {
			return nil, err
		}

		bookings = append(bookings, booking)
	}

	return bookings, nil
}

func GetLocations() ([]models.Localization, error) {
	rows, err := db.Query("SELECT * FROM cr_localization")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var localizations []models.Localization
	for rows.Next() {
		var localization models.Localization
		err := rows.Scan(
			&localization.Loc_id,
			&localization.Building,
			&localization.Floor,
			&localization.Name,
		)
		if err != nil {
			return nil, err
		}
		localizations = append(localizations, localization)
	}

	return localizations, nil
}

func GetRooms() ([]models.Localization, error) {
	rows, err := db.Query("SELECT * FROM conference_room")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var localizations []models.Localization
	for rows.Next() {
		var localization models.Localization
		err := rows.Scan(
			&localization.Loc_id,
			&localization.Building,
			&localization.Floor,
			&localization.Name,
		)
		if err != nil {
			return nil, err
		}
		localizations = append(localizations, localization)
	}

	return localizations, nil
}

// GetRoomsByLocalization retrieves conference rooms based on localization.
func GetRoomsByLocalization(localizationID int) ([]models.ConferenceRoom, error) {
	rows, err := db.Query("SELECT * FROM conference_room WHERE localization = ?", localizationID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var conferenceRooms []models.ConferenceRoom
	for rows.Next() {
		var room models.ConferenceRoom
		err := rows.Scan(
			&room.RoomID,
			&room.Name,
			&room.RoomDescription,
			&room.Delegation,
			&room.Localization,
			&room.RoomPriority,
			&room.PermanentEquipment,
			&room.Created,
			&room.CreatedBy,
			&room.Updated,
			&room.UpdatedBy,
		)
		if err != nil {
			return nil, err
		}
		conferenceRooms = append(conferenceRooms, room)
	}

	return conferenceRooms, nil
}

// CreateBooking creates a new booking in the database and checks for time conflicts.
func CreateBooking(newBooking models.Booking) error {
	// Check for time conflicts before inserting the new booking
	if err := checkTimeConflicts(newBooking.RoomID, newBooking.BookingDateStart, newBooking.BookingDateEnd); err != nil {
		return err
	}

	// If no conflicts, insert the new booking
	_, err := db.Exec(
		"INSERT INTO booked_rooms (title_meeting, participants, user_id, room_id, status, "+
			"booking_date_start, booking_date_end, booking_params, optionals) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
		newBooking.TitleMeeting,
		newBooking.Participants,
		newBooking.UserID,
		newBooking.RoomID,
		newBooking.Status,
		newBooking.BookingDateStart,
		newBooking.BookingDateEnd,
		newBooking.BookingParams,
		newBooking.Optionals,
	)
	if err != nil {
		return err
	}

	return nil
}

// checkTimeConflicts checks for time conflicts with existing bookings in the specified room.
func checkTimeConflicts(roomID int, startTime, endTime time.Time) error {
	query := `
        SELECT COUNT(*)
        FROM booked_rooms
        WHERE room_id = ? AND
              (booking_date_start BETWEEN ? AND ? OR
               booking_date_end BETWEEN ? AND ? OR
               ? BETWEEN booking_date_start AND booking_date_end)
    `

	var count int
	err := db.QueryRow(query, roomID, startTime, endTime, startTime, endTime, startTime).Scan(&count)
	if err != nil {
		return err
	}

	if count > 0 {
		return fmt.Errorf("conflict with existing bookings in the room")
	}

	return nil
}

// GetAvailableRooms retrieves available rooms based on the provided criteria.
func GetAvailableRooms(start time.Time, end time.Time, participants int, localization string) ([]byte, error) {
	// Implement the SQL query to check room availability

	// Example query, modify according to your database schema
	query := `
        SELECT
            room_id,
            name AS room_name,
            room_description,
            delegation,
            localization,
            room_priority,
            permanent_equipment,
            created,
            created_by,
            updated,
            updated_by
        FROM
            conference_room cr
        WHERE
            localization = ?
            AND room_id NOT IN (
                SELECT
                    room_id
                FROM
                    booked_rooms br
                WHERE
                    (? BETWEEN br.booking_date_start AND br.booking_date_end)
                    OR (? BETWEEN br.booking_date_start AND br.booking_date_end)
                    OR (br.booking_date_start BETWEEN ? AND ?)
                    OR (br.booking_date_end BETWEEN ? AND ?)
            )
            AND cr.max_capacity >= ?
    `

	rows, err := db.Query(query, localization, start, end, participants)
	if err != nil {
		log.Println("Error executing query:", err)
		return nil, err
	}
	defer rows.Close()

	var availableRooms []models.ConferenceRoom
	for rows.Next() {
		var room models.ConferenceRoom
		// Scan columns from the result set and populate the room struct
		if err := rows.Scan(&room.RoomID, &room.RoomDescription, &room.Delegation, &room.Localization, &room.RoomPriority, &room.PermanentEquipment, &room.Created, &room.CreatedBy, &room.Updated, &room.UpdatedBy); err != nil {
			log.Println("Error scanning row:", err)
			return nil, err
		}

		availableRooms = append(availableRooms, room)
	}

	// Convert the result to JSON without nulls
	jsonResult, err := json.Marshal(availableRooms)
	if err != nil {
		log.Println("Error marshaling JSON:", err)
		return nil, err
	}

	return jsonResult, nil
}
