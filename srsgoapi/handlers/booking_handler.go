// handlers/booking_handler.go
package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"srsgoapi/db"
	"srsgoapi/models"
	"strconv"
	"time"
)

// GetBookingHandler retrieves all bookings from the database and sends them as JSON.
func GetBookingHandler(w http.ResponseWriter, r *http.Request) {
	// Implement logic to retrieve bookings from the database
	bookings, err := db.GetBookings()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(bookings)
}

// GetBookingHandler retrieves all bookings from the database and sends them as JSON.
func GetLocalizationsHandler(w http.ResponseWriter, r *http.Request) {
	// Implement logic to retrieve bookings from the database
	localizations, err := db.GetLocations()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(localizations)
}

// GetRooms retrieves all bookings from the database and sends them as JSON.
func GetRoomsHandler(w http.ResponseWriter, r *http.Request) {
	// Implement logic to retrieve bookings from the database
	localizations, err := db.GetRooms()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(localizations)
}

func GetRoomsByLocalizationHandler(w http.ResponseWriter, r *http.Request) {
	// Parse the localization ID parameter from the query string
	locIDStr := r.URL.Query().Get("localization_id")
	if locIDStr == "" {
		http.Error(w, "Localization ID parameter is required", http.StatusBadRequest)
		return
	}

	// Convert the localization ID string to an integer
	localizationID, err := strconv.Atoi(locIDStr)
	if err != nil {
		http.Error(w, "Invalid localization ID", http.StatusBadRequest)
		return
	}

	// Retrieve conference rooms based on localization from the database
	conferenceRooms, err := db.GetRoomsByLocalization(localizationID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return the conference rooms as JSON
	json.NewEncoder(w).Encode(conferenceRooms)
}

// CreateBookingHandler handles the submission of the add booking form with JSON data.
func CreateBookingHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// Parse JSON from the request body
	var newBooking models.Booking
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&newBooking)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Create the booking in the database
	err = db.CreateBooking(newBooking)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Booking created successfully"))
}

func CheckAvailabilityHandler(w http.ResponseWriter, r *http.Request) {
	// Parse request parameters
	startStr := r.URL.Query().Get("start")
	start, err := time.Parse("2006-01-02T15:04:05Z", startStr)
	if err != nil {
		fmt.Println(startStr)
		http.Error(w, "Invalid start date", http.StatusBadRequest)
		return
	}

	endStr := r.URL.Query().Get("start")
	end, err := time.Parse("2006-01-02T15:04:05Z", endStr)
	if err != nil {
		fmt.Println(endStr)
		http.Error(w, "Invalid start date", http.StatusBadRequest)
		return
	}

	participants, err := strconv.Atoi(r.URL.Query().Get("participants"))
	if err != nil {
		http.Error(w, "Invalid participants value", http.StatusBadRequest)
		return
	}

	localization := r.URL.Query().Get("localization")

	// Get available rooms
	availableRooms, err := db.GetAvailableRooms(start, end, participants, localization)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Error getting available rooms:", err)
		return
	}

	// Convert the result to JSON without nulls
	jsonResult, err := json.Marshal(availableRooms)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Error marshaling JSON:", err)
		return
	}

	// Set Content-Type header
	w.Header().Set("Content-Type", "application/json")

	// Write the JSON response
	w.Write(jsonResult)
}
