# Room Booking App Diagram


## Docker setup:

### Commands 

    Running composition
    ```
    docker-compose up -d
    ```
    Stop it
    ```
    docker-compose down
    ```
   Specific:
   ```
   docker compose -f docker-compose.dev.yml up -d
   docker compose -f docker-compose.health.yml up -d
   docker compose -f docker-compose.yml run up -d
   ```


## Components:

### 1. Frontend (UI)
   - User Input Forms
   - Room Selection Interface
   - Booking Confirmation Screen
   - Client-Side Validation
   - API Requests (CRUD Operations)
   - Display API Responses
   - User Interaction Handling

### 2. Backend (Server)
   - API Routes
     - POST `/api/rooms` *(Create)*
     - GET `/api/rooms` *(Read)*
     - PUT `/api/rooms/{id}` *(Update)*
     - DELETE `/api/rooms/{id}` *(Delete)*
   - Business Logic *(Booking Logic, Validation)*
   - Database Interaction *(CRUD Operations)*
  
### 3. Database
   - Rooms Table
     - ID (Primary Key)
     - Name
     - Capacity
     - Features
     - Booking Number
     - Availability Status
     - Location


## More API

### 1. Create Booking:

- Endpoint: /bookings/create
   **Method: POST**
   Purpose: Create a new booking.
   Get All Bookings:

- Endpoint: /bookings
   **Method: GET**
   Purpose: Retrieve a list of all bookings.
   Get Booking by ID:

- Endpoint: /bookings/{id}
   **Method: GET**
   Purpose: Retrieve details of a specific booking by its ID.
   Update Booking:

- Endpoint: /bookings/update/{id}
   **Method: PUT/PATCH**
   Purpose: Update details of a specific booking.
   Delete Booking:

- Endpoint: /bookings/delete/{id}
   **Method: DELETE**
   Purpose: Delete a specific booking.
   Get Available Rooms:

- Endpoint: /rooms/available
   **Method: GET**
   Purpose: Retrieve a list of available rooms for booking.
   Get Rooms by Localization:

- Endpoint: /rooms/localization/{localization}
   **Method: GET**
   Purpose: Retrieve rooms based on their localization.
   Get Booking by User:

- Endpoint: /bookings/user/{user_id}
   **Method: GET**
   Purpose: Retrieve bookings associated with a specific user.
   Get Booking by Week:

- Endpoint: /bookings/week/{date}
   **Method: GET**
   Purpose: Retrieve bookings for a specific week based on a given date.
   Get Room Details:

- Endpoint: /rooms/{room_id}
   **Method: GET**
   Purpose: Retrieve details of a specific room.
   Get Room Availability:

- Endpoint: /rooms/availability/{date}
   **Method: GET**
   Purpose: Retrieve room availability for a specific date.

## Workflow:

1. **User selects a room and provides booking details in the UI.**

2. **Frontend validates the user input.**
   - Ensure all necessary fields are filled.
   - Validate the correctness of the input data.

3. **Frontend sends a request to the Backend API to create a booking.**
   - POST `/api/rooms`

4. **Backend processes the request.**
   - Validates the data again for security.
   - Checks room availability.
   - If available, updates the database, marking the room as booked.

5. **Backend sends a response to the Frontend.**
   - Indicates success or failure.
   - If successful, includes booking details.

6. **Frontend displays the confirmation screen to the user.**

7. **User can view their bookings or make new bookings.**

8. **For modifications or cancellations:**
   - User interacts with the UI, updating booking details.
   - Frontend sends appropriate requests to the Backend.
   - Backend processes requests, updates the database accordingly.

9. **Backend and Frontend communicate for any additional actions, ensuring data consistency and user feedback.**

## Notes:

- **Error Handling:**
  - Both frontend and backend should handle errors gracefully, providing meaningful messages to the user.

- **Security:**
  - Implement authentication and authorization mechanisms to secure API endpoints.
  - Use secure protocols for data transmission (e.g., HTTPS).

- **Scalability:**
  - Design the system to handle a large number of concurrent users and bookings.

- **Logging and Monitoring:**
  - Implement logging mechanisms to track user activities and errors.
  - Set up monitoring tools to detect and respond to system issues.


