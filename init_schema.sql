-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: bookings
-- ------------------------------------------------------
-- Server version 5.5.5-10.10.6-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `booked_rooms`
--

DROP TABLE IF EXISTS `booked_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `booked_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_meeting` varchar(255) DEFAULT NULL,
  `participants` int(22) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `room_id` int(11) NOT NULL DEFAULT 0,
  `status` varchar(255) NOT NULL DEFAULT '',
  `booking_date_start` datetime NOT NULL,
  `booking_date_end` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `booking_params` int(11) NOT NULL DEFAULT 0,
  `optionals` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_room_id` FOREIGN KEY (`room_id`) REFERENCES `conference_room` (`room_id`),
  CONSTRAINT `fk_status` FOREIGN KEY (`status`) REFERENCES `booking_status` (`id`),
  CONSTRAINT `fk_booking_params` FOREIGN KEY (`booking_params`) REFERENCES `room_parameters` (`id`),
  CONSTRAINT `fk_optionals` FOREIGN KEY (`optionals`) REFERENCES `optional_equipment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `conference_room`;
CREATE TABLE IF NOT EXISTS `conference_room` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(255) NOT NULL DEFAULT '',
  `room_description` text NOT NULL,
  `room_localization` char(255) NOT NULL DEFAULT '',
  `delegation` bool NOT NULL DEFAULT '0',
  `maximum_capacility` int(11) NOT NULL DEFAULT '0',
  `room_priority` int(11) NOT NULL DEFAULT '0',
  `permanent_equipment` int(11) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `permanent_equipment`;
CREATE TABLE IF NOT EXISTS `permanent_equipment` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `screen` tinyint(1) NOT NULL DEFAULT 0,
  `projector` tinyint(1) NOT NULL DEFAULT 0,
  `numbers_of_microphones` tinyint(1) NOT NULL DEFAULT 0,
  `microphone_stand` tinyint(1) NOT NULL DEFAULT 0,
  `sound_connection` tinyint(1) NOT NULL DEFAULT 0,
  `rostrum` tinyint(1) NOT NULL DEFAULT 0,
  `internet_wifi` tinyint(1) NOT NULL DEFAULT 0,
  `internet_eth` tinyint(1) NOT NULL DEFAULT 0,
  `miejska` tinyint(1) NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `optional_equipment`;
CREATE TABLE IF NOT EXISTS `optional_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `laptop` tinyint(1) NOT NULL DEFAULT 0,
  `projection_screen` tinyint(1) NOT NULL DEFAULT 0,
  `remote_control` tinyint(1) NOT NULL DEFAULT 0,
  `projector` tinyint(1) NOT NULL DEFAULT 0,
  `battery_sound_system` tinyint(1) NOT NULL DEFAULT 0,
  `computers_speakers` tinyint(1) NOT NULL DEFAULT 0,
  `usb_microphone` tinyint(1) NOT NULL DEFAULT 0,
  `dictaphone` tinyint(1) NOT NULL DEFAULT 0,
  `flipchart` tinyint(1) NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `room_parameters`;
CREATE TABLE IF NOT EXISTS `room_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `availability` int(11) NOT NULL DEFAULT '0',
  `session` tinyint(1) NOT NULL DEFAULT 0,
  `setting` int(11) NOT NULL DEFAULT '0',
  `tables` int(11) NOT NULL DEFAULT '0',
  `cathering` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`availability`) REFERENCES `rp_availability` (`id`),
  FOREIGN KEY (`setting`) REFERENCES `rp_settings` (`id`),
  FOREIGN KEY (`tables`) REFERENCES `rp_tables` (`id`),
  FOREIGN KEY (`cathering`) REFERENCES `rp_catering` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL DEFAULT '',
  `role` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`role`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `booking_status`;
CREATE TABLE IF NOT EXISTS `booking_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `rp_settings`;
CREATE TABLE IF NOT EXISTS `rp_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `rp_tables`;
CREATE TABLE IF NOT EXISTS `rp_tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `rp_catering`;
CREATE TABLE IF NOT EXISTS `rp_catering` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL DEFAULT '',
  `created` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `rp_availability`;
CREATE TABLE IF NOT EXISTS `rp_availability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;