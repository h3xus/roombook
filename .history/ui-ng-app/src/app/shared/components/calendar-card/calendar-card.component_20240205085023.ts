
import { Attribute, ChangeDetectionStrategy, Component, HostBinding, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { formatDistance } from 'date-fns';
import { pl } from 'date-fns/locale';
import { TimefmtPipe } from "@shared/pipes/timefmt.pipe";

@Component({
    standalone: true,
    selector: 'app-calendar-card',
    templateUrl: './calendar-card.component.html',
    // styleUrls: ['./calendar-card.component.css'],

  styles: [`
    :host {
      display: flex;
    }
  `],
    host: { 'class': 'row-start-[11] row-span-6 bg-blue-400/20 dark:bg-sky-600/50 border border-blue-700/10 dark:border-sky-500 rounded-lg m-1 p-1 flex flex-col' },
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [TimefmtPipe]
})

export class CalendarCardComponent implements OnInit{

  @Input() start: string = '0';
  @Input() end: string = '1';
  headingColor: string = 'Black';
  meetingTime: any;

  hostClass = {
    'foo': true,
    'bar': false,
  }
  ngOnInit() {
    this.meetingTime = formatDistance(new Date(this.start), new Date(this.end), { locale: pl })
  }
  // @HostBinding('class') get HeadingClass() {
  //   return this.headingColor
  // }

  // constructor(
  //   @Attribute('class') public classNames: string
  // ) {
  //   console.log(this.classNames);
  // }
}
// import { Component, ElementRef, HostBinding, Input, OnInit, ViewEncapsulation } from '@angular/core';
// import { CommonModule } from '@angular/common';

// @Component({
//   selector: 'app-calendar-card',
//   standalone: true,
//   imports: [CommonModule],
//   templateUrl: './calendar-card.component.html',
//   styleUrl: './calendar-card.component.css',
// })
// export class CalendarCardComponent implements OnInit {
//   @Input({ required: true }) x!: number;
//   @Input({ required: true }) y!: number;
//   @HostBinding('class.row-start-[1]') myCustomClass = true;
//   @HostBinding('class.col-start-[1]') newCustomClass = true;

//   ngOnInit(): void {

//   }
  
//   constructor(private el: ElementRef) {
//     // Access the root element directly using ElementRef
//     const cls = ['row-span-1', 'bg-blue-400/20', 'dark:bg-sky-600/50', 'border', 'border-blue-700/10', 'dark:border-sky-500', 'rounded-lg', 'm-1', 'p-1', 'flex', 'flex-col']
//     el.nativeElement.classList.add(cls);
//   }

// }
