import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: 'dashboard',
    loadComponent: () => import('./dashboard/dashboard.component'),
    children: [
      {
        path: 'bookings-list',
        title: 'Bookings list',
        loadComponent: () =>
          import('./dashboard/pages/bookings-list/bookings-list.component'),
      },
      {
        path: 'new-booking',
        title: 'New booking',
        loadComponent: () =>
          import('./dashboard/pages/new-booking/new-booking.component'),
      },
      {
        path: 'change-detection',
        title: 'Change Detection',
        loadComponent: () =>
          import('./dashboard/pages/change-detection/change-detection.component'),
      },
      {
        path: 'locations',
        title: 'locations',
        loadComponent: () =>
          import('./dashboard/pages/locations/locations.component'),
      },
      {
        path: 'events',
        title: 'Events',
        loadComponent: () =>
          import('./dashboard/pages/events/events.component'),
      },
      {
        path: 'raports',
        title: 'Raports',
        loadComponent: () =>
          import('./dashboard/pages/raports/raports.component'),
      },
      {
        path: 'control-flow',
        title: 'Control Flow',
        loadComponent: () =>
          import('./dashboard/pages/control-flow/control-flow.component'),
      },
      {
        path: 'defer-options',
        title: 'Defer Options',
        loadComponent: () =>
          import('./dashboard/pages/defer-options/defer-options.component'),
      },
      {
        path: 'defer-views',
        title: 'Defer Views',
        loadComponent: () =>
          import('./dashboard/pages/defer-views/defer-views.component'),
      },
      {
        path: 'user/:id',
        title: 'User View',
        loadComponent: () => import('./dashboard/pages/user/user.component'),
      },
      {
        path: 'user-list',
        title: 'User List',
        loadComponent: () => import('./dashboard/pages/users/users.component'),
      },
      {
        path: 'view-transition',
        title: 'View Transition',
        loadComponent: () =>
          import('./dashboard/pages/view-transition/view-transition.component'),
      },
      {
        path: 'calendar',
        title: 'Calendar',
        loadComponent: () => import('./dashboard/pages/calendar/calendar.component'),
      },
      {
        path: '',
        redirectTo: 'events',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
  },
];
