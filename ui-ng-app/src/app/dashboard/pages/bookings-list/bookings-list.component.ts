import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  standalone: true,
  imports: [CommonModule],
  templateUrl: './bookings-list.component.html',
  styles: [``],
})
export default class BookingsListComponent {}
