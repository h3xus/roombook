import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartComponent } from '@shared/components/chart/chart.component';

@Component({
  standalone: true,
  imports: [CommonModule, ChartComponent],
  templateUrl: './raports.component.html',
  styles: [``],
})
export default class RaportsComponent {}
