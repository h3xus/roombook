import { Component, OnInit, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingsService } from '@shared/services/bookings.service'
import { HttpClientModule } from '@angular/common/http';

@Component({
  standalone: true,
  imports: [CommonModule, HttpClientModule],
  templateUrl: './change-detection.component.html',
  providers: [BookingsService],
  styles: [``],
})
export default class ChangeDetectionComponent implements OnInit {

  public bookings = signal([]);
  constructor(private BookingsService: BookingsService) {}

  public users = [];
  public frameworks = signal(['Angular', 'Vue', 'Svelte', 'Qwik', 'React']);

  ngOnInit(): void {
    this.BookingsService.getBookings().subscribe((bookingsResponse: any) => {
      console.log('Response from service getAllbookings', bookingsResponse);
      this.bookings.set(bookingsResponse);
      this.users = bookingsResponse.bookings
    });
  }
  fetchData() {
    this.BookingsService.getBookings().subscribe((bookingsResponse: any) => {
      console.log('Response from service getAllbookings', bookingsResponse);
      this.frameworks.set(bookingsResponse.bookings);
      this.users = bookingsResponse.bookings
    });
  }
}

