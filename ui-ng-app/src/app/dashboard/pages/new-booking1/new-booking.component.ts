import { Component, OnInit, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { PermequipmentComponent } from '@shared/components/permequipment/permequipment.component';
import { OptionalequipmentComponent } from '@shared/components/optionalequipment/optionalequipment.component';
import { formatRelative, addDays, subDays, format } from 'date-fns'
import { pl } from 'date-fns/locale'
import { FormBuilder, FormGroup, ReactiveFormsModule, FormControl } from '@angular/forms';
import { SchedulerComponent } from "@shared/components/scheduler/scheduler.component";
import { DatePlaceholderPipe } from '@shared/pipes/date-placeholder.pipe';
import {
  Datetimepicker,
  Input,
  initTE,
} from "tw-elements";

@Component({
  standalone: true,
  templateUrl: './new-booking.component.html',
  styles: [``],
  imports: [CommonModule, PermequipmentComponent, OptionalequipmentComponent, ReactiveFormsModule, SchedulerComponent, DatePlaceholderPipe]
})
export default class NewBookingComponent implements OnInit {
booking_date_end: FormControl<any> | undefined;
// booking_date_start: FormControl<any>;

  constructor(private formBuilder: FormBuilder) { }
  meetingForm = new FormGroup({
    'title_meeting': new FormControl(''),
    'participants': new FormControl(''),
    'user_id': new FormControl(''),
    'room_id': new FormControl(''),
    'status': new FormControl(''),
    'booking_date_start': new FormControl(new Date()),
    'booking_date_end': new FormControl(new Date()),
    'booking_params': new FormControl(''),
    'optionals': new FormControl(''),
  })
  private ptSettings = {
    "datepicker":{
      "startDay": 1,
      "title": "wybierz datę",
      "weekdaysNarrow": ['Nd','P', 'W', 'Ś', 'Cz', 'Pt', 'Sob'],
      "cancelBtnText":"Anuluj",
      "clearBtnText":"Czyść",
      "weekdaysShort": ['Nd', 'P', 'W', 'Ś', 'Cz', 'Pt', 'Sob'],
      "monthsShort": ['Sty', 'Lu', 'Mar', 'Kw', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
      "monthsFull": ['Styczeń', 'Luty', 'Marzec', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    },
    "timepicker":{
      "format24": true,
      "inline": true,
    }
  }

  ngOnInit(): void {
    initTE({ Input });
    const ptOpts1 = document.querySelector('#datestart');
    const ptOpts2 = document.querySelector('#dateend');
    new Datetimepicker(ptOpts1, this.ptSettings);
    new Datetimepicker(ptOpts2, this.ptSettings);
  }

  public showMore = signal(false);
  public showContent = signal(false);
  public x = formatRelative(subDays(new Date(), -1), new Date(), { locale: pl })
  public tomorrow = format(new Date().setDate(new Date().getDate() + 1), "yyyy-MM-dd")
  public dayAfterTomorrow = format(addDays(this.tomorrow, 1), "yyyy-MM-dd")
  public toggleMore() {
    this.showMore.update((value) => !value);
  }
  public toggleContent() {
    this.showContent.update((value) => !value);
  }

  combineDateAndTime(date: string | null | undefined, time: string | null | undefined) {
    const combinedDateTime = new Date(`${date}T${time}:00Z`).toISOString();
    return combinedDateTime;
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.meetingForm.value);
    // this.sendDataToAPI(data)
    console.log(this.booking_date_end)
    if (this.meetingForm.valid) {
      // const combinedDateTime = this.combineDateAndTime(this.meetingForm.value.start_date, this.meetingForm.value.start_time);
      // this.sendDataToAPI({ booking_date_start: combinedDateTime });
    }
  }
  // sendDataToAPI(data: any) {
  //   this.http.post('/api/upload', data, {
  //     reportProgress: true,
  //     observe: 'events',
  //   }).subscribe(event => {
  //     switch (event.type) {
  //       case HttpEventType.Response:
  //         console.log(Response);
  //         break;

  //     }
  //   });
  // }
}
