import { Component, OnInit, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingsService } from '@shared/services/bookings.service'
import { HttpClientModule } from '@angular/common/http';
import { intervalToDuration,format } from 'date-fns';

import { TimedurationPipe } from '@shared/pipes/timeduration.pipe';
import { TimefmtPipe } from '@shared/pipes/timefmt.pipe';
import { DatefmtplPipe } from '@shared/pipes/datefmtpl.pipe';

@Component({
  standalone: true,
  imports: [CommonModule, HttpClientModule, TimedurationPipe, TimefmtPipe, DatefmtplPipe],
  templateUrl: './events.component.html',
  providers: [BookingsService],
  styles: [``] 
})
export default class EventsComponent implements OnInit {
  public duration(startDate:Date, endDATE:Date){
    return intervalToDuration({ start:startDate, end: endDATE })
  }

  public bookings = signal([]);
  constructor(private BookingsService: BookingsService) { }

  ngOnInit(): void {
    this.BookingsService.getBookings().subscribe((bookingsResponse: any) => {
      console.log('Response from service getAllbookings', bookingsResponse);
      this.bookings.set(bookingsResponse);
    });
  }
}
