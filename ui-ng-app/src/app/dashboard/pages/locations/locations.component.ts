import { Component, OnInit, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingsService } from '@shared/services/bookings.service';
import { HttpClientModule } from '@angular/common/http';

@Component({
  standalone: true,
  imports: [CommonModule,HttpClientModule],
  templateUrl: './locations.component.html',
  providers: [BookingsService],
  styles: [``],
})
export default class LocationsComponent implements OnInit{
  public locations = signal([]);
  constructor(private BookingsService: BookingsService) {}

  ngOnInit(): void {
    this.BookingsService.getLocations().subscribe((locationsResponse: any) => {
      console.log('Response from service getAllLocations', locationsResponse);
      this.locations.set(locationsResponse);
    });
  }
}
