import { Component, OnInit, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { isToday, isMonday, addDays, eachDayOfInterval, startOfMonth, previousMonday, nextSunday, endOfMonth, format, isSunday, endOfWeek, isSameDay, getDay } from 'date-fns'
import { pl, th } from 'date-fns/locale'
import { SchedulerComponent } from '@shared/components/scheduler/scheduler.component';
import { CalendarGenComponent } from '@shared/components/calendar-gen/calendar-gen.component';
import { DayPlannerComponent } from "@shared/components/day-planner/day-planner.component";
import { BookingsService } from '@shared/services/bookings.service'
import { HttpClientModule } from '@angular/common/http';

type CalView = 'Day' | 'Week' | 'Month';

export interface meetingData {
  booking_date_start: Date | string | number;
  booking_date_end: Date | string | number;
  booking_room: string;
  booking_title: string;
  booking_description: string;
}

@Component({
  standalone: true,
  templateUrl: './calendar.component.html',
  styles: [``],
  providers: [BookingsService],
  imports: [CommonModule, HttpClientModule, SchedulerComponent, CalendarGenComponent, DayPlannerComponent]
})
export default class CalendarComponent implements OnInit {
  public Today = new Date();
  public currentDate = this.Today;
  public currentMonth = format(this.currentDate, 'LLLL yyyy', { locale: pl });
  public firstDayOfMonth = startOfMonth(this.currentDate);
  public lastDayOfMonth = endOfMonth(this.currentDate);
  public view = signal<CalView>('Week');

  public changeView(par: CalView) {
    this.view.set(par);
  }

  public calendarDays = eachDayOfInterval({ start: this.firstDayOfMonth, end: this.lastDayOfMonth })
    .map((day) => ({
      date: day,
      num: format(day, 'dd'),
      isToday: isToday(day),
      weekDay: this.getDayNum(day),
      isMonday: isMonday(day),
      isSunday: isSunday(day),
      endOfWeek: endOfWeek(day)
    }));

  // if Today is Monday don't check for last Monday
  public thisWeek = eachDayOfInterval(
    { start: isMonday(previousMonday(this.currentDate)) ? this.currentDate : previousMonday(this.currentDate), end: nextSunday(this.currentDate) },
  ).map((day) => ({
    date: day,
    dayName: format(day, 'EE', { locale: pl }),
    weekDay: this.getDayNum(day),
    isToday: isToday(day),
  })).slice(0, 7);

  public getDayNum(day: Date) {
    if (getDay(new Date(day)) == 0) {
      return 7
    } else {
      return getDay(new Date(day))
    }
  }

  // public aWeekFromNow = addDays(this.currentDate, 7);
  public bookings: meetingData[] = [];

  constructor(private BookingsService: BookingsService) { }

  ngOnInit() {
    this.BookingsService.getBookings().subscribe((bookingsResponse: any) => {
      console.log('Response from service getAllbookings', bookingsResponse);
      this.bookings = bookingsResponse;

      // Update calendarDaysWithMeetings after fetching data
      this.updateCalendarDaysWithMeetings();
      this.updateWeeklyMeetings();
    });
  }
  
  public calendarDaysWithMeetings: object[] | undefined;
  private updateCalendarDaysWithMeetings() {
    this.calendarDaysWithMeetings = this.calendarDays.map((day) => ({
      ...day,
      hasMeetings: this.hasMeetingsOnDay(day.date),
    }));
  }

  public weekDaysWithMeetings: object[] | undefined;
  private updateWeeklyMeetings() { 
    this.weekDaysWithMeetings = this.thisWeek.map((day) => ({
      ...day,
      hasMeetings: this.hasMeetingsOnDay(day.date),
    }));
  }



  public hasMeetingsOnDay(day: Date): object {
    const meetingsForDay = this.bookings.filter((meeting: { booking_date_start: Date | string | number }) =>
      isSameDay(new Date(meeting.booking_date_start), new Date(day))
    );
    return meetingsForDay;
  }

  // calendar navigation
  public next(time:string): void {
    return this.changeMonth(time);
  }
  public previous(time:string): void {
    return this.changeMonth(time);
  }

  changeWeek(time:string): void {
   console.log(time);
  }
  changeMonth(time: string): void {
    throw new Error('Method not implemented.');
  }

}
