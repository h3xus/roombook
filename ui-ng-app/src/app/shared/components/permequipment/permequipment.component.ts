import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-permequipment',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './permequipment.component.html',
  styleUrl: './permequipment.component.css'
})
export class PermequipmentComponent {

}
