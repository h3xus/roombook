import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatefmtplPipe } from '@shared/pipes/datefmtpl.pipe';
import { isTodayDirective } from '@shared/directives/is-today.directive';

@Component({
  selector: 'app-calendar-gen',
  standalone: true,
  imports: [CommonModule, DatefmtplPipe, isTodayDirective],
  templateUrl: './calendar-gen.component.html',
  styleUrl: './calendar-gen.component.css'
})

export class CalendarGenComponent implements OnInit{
  @Input({ required: false }) Week: any;
  @Input({ required: false }) Month: any;
  
  ngOnInit(): void {
    console.log(this.Week, this.Month);
  }
}
