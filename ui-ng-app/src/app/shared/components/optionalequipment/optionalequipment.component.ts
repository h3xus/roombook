import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-optionalequipment',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './optionalequipment.component.html',
  styleUrl: './optionalequipment.component.css'
})
export class OptionalequipmentComponent {

}
