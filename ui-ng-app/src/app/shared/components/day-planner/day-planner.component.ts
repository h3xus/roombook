import { Component,Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { format, startOfMonth, eachDayOfInterval, endOfMonth, isSameMonth } from 'date-fns';
// import { CalendarCardComponent } from '@shared/components/calendar-card/calendar-card.component';

@Component({
  selector: 'app-day-planner',
  standalone: true,
  // imports: [CalendarCardComponent],
  templateUrl: './day-planner.component.html',
  styleUrl: './day-planner.component.css'
})
export class DayPlannerComponent implements OnInit{
  @Input({ required: false }) weekName!: string;
  @Input({ alias: 'week'})
  // public days: Date[] = [];
  public currentMonth = Date;
  public selectedDate = Date;
  // public currentDate = new Date();

  ngOnInit() {
    // this.renderCalendar();
  }

  // createCalendar(month: Date, selectedDate: Date) {
  //   const firstDayOfMonth = startOfMonth(month);
  //   const lastDayOfMonth = endOfMonth(month);
  //   this.days = eachDayOfInterval({ start: firstDayOfMonth, end: lastDayOfMonth });
  // }

  // renderCalendar() {
  //   this.currentMonth = startOfMonth(this.currentDate);
  //   selectedDate = this.currentDate;
  //   this.createCalendar(currentMonth, this.selectedDate);
  // }

  // // Function to determine classes for each day
  // dayClasses(day: Date): string {
  //   const isCurrentMonth = isSameMonth(day, currentMonth);
  //   const isSelected = isSameMonth(day, selectedDate) && day.getDate() === this.selectedDate.getDate();
  //   return `
  //     ${isCurrentMonth ? 'text-black' : 'text-gray-400'}
  //     ${isSelected ? 'bg-blue-500 text-white' : ''}
  //     p-2 rounded
  //   `;
  // }

  // // Function to handle day click event
  // onDayClick(day: Date): void {
  //   if (isSameMonth(day, this.currentMonth)) {
  //     this.selectedDate = day;
  //   }
  // }
}