import { Component, Input, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { startOfMonth, eachDayOfInterval, endOfMonth, isSameMonth, getISOWeek, intervalToDuration, getHours } from 'date-fns';
import { CalendarCardComponent } from '@shared/components/calendar-card/calendar-card.component';
import { DatefmtplPipe } from '@shared/pipes/datefmtpl.pipe';
import { isTodayDirective } from '@shared/directives/is-today.directive';


@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css'],
  standalone: true,
  imports: [CalendarCardComponent, CommonModule, DatefmtplPipe, isTodayDirective],
})
export class SchedulerComponent implements OnInit {
  @Input({ required: false }) Week: any;
  @Input({ required: false }) Meetings: any;
  @Input({ required: false }) Hours: number | undefined;
  public weekOfYear = getISOWeek(new Date());
  public currentMonth = new Date();
  public selectedDate = new Date();

  public generateTimeArray() {
    const times = [];

    for (let hours = 0; hours < 24; hours++) {
      for (let minutes = 0; minutes < 60; minutes += 30) {
        const hourString = hours.toString().padStart(2, '0');
        const minuteString = minutes.toString().padStart(2, '0');
        const time = `${hourString}:${minuteString}`;
        times.push(time);
      }
    }

    return times;
  }
  public meetingStart(startDate: Date): number {
    let time = getHours(startDate);
        time = time * 2;
    return time + 1;
  }

  public duration(startDate: Date, endDATE: Date): string{
    let values:any = intervalToDuration({ start: startDate, end: endDATE });

    if(values.minutes) {
      values = values.hours *2 + 1 
    } else {
      values = values.hours *2
    }
    console.log(startDate,values);
    return values
  }

  public timeArray: string[] | undefined;

  ngOnInit() {
    this.renderCalendar();
    this.weekOfYear = getISOWeek(this.Week[0].date);
    console.log(this.Meetings);
    this.timeArray = this.generateTimeArray();
  }

  createCalendar(month: Date, selectedDate: Date) {
    const firstDayOfMonth = startOfMonth(month);
    const lastDayOfMonth = endOfMonth(month);
    const days = eachDayOfInterval({ start: firstDayOfMonth, end: lastDayOfMonth });
    return days;
  }

  renderCalendar() {
    this.currentMonth = startOfMonth(new Date());
    this.selectedDate = new Date();
    const days = this.createCalendar(this.currentMonth, this.selectedDate);
  }

  dayClasses(day: Date): string {
    const isCurrentMonth = isSameMonth(day, this.currentMonth);
    const isSelected = isSameMonth(day, this.selectedDate) && day.getDate() === this.selectedDate.getDate();
    return `
        ${isCurrentMonth ? 'text-black' : 'text-gray-400'}
        ${isSelected ? 'bg-blue-500 text-white' : ''}
        p-2 rounded
      `;
  }

  onDayClick(day: Date): void {
    if (isSameMonth(day, this.currentMonth)) {
      this.selectedDate = day;
    }
  }
}
