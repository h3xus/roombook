import { Directive, Input, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appTodayClass]',
  standalone: true
})
export class isTodayDirective {

  @Input() set appTodayClass(condition: boolean) {
    if (condition) {
      this.renderer.addClass(this.elementRef.nativeElement, 'today');
    }
  }

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }
}
