import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class BookingsService {

    private baseUrl: string;
    private bookingsUrl: string;
    private allLocationsUrl: string;

    constructor(private http: HttpClient) {
        this.baseUrl = 'http://localhost:7075/';
        this.bookingsUrl = 'bookings';
        this.allLocationsUrl = 'rooms/locations/all';
    }
    getBookings(id?: string, title_meeting?: string, booking_date_start?: string, booking_date_end?: string) {
        return this.http.get(this.baseUrl+this.bookingsUrl);
    }
    getLocations() {
        return this.http.get(this.baseUrl+this.allLocationsUrl);
    }
}