import { Injectable, signal} from '@angular/core';
import { parse, getISOWeek } from 'date-fns';
import { startOfDay, format, startOfMonth, eachDayOfInterval, endOfMonth, isToday,isMonday } from 'date-fns'
import { pl } from 'date-fns/locale'

@Injectable({
  providedIn: 'root'
})
export class ClockworkService {
  public today = startOfDay(new Date());
  public currentDate = signal<Date>(this.today);
  public firstDayOfMonth = startOfMonth(this.currentDate());
  public lastDayOfMonth = endOfMonth(this.currentDate());
  public calendarDays = eachDayOfInterval({ start: this.firstDayOfMonth, end: this.lastDayOfMonth })
    .map((day) => ({
      date: format(day, 'EEEE'),
      isToday: isToday(day),
      day: format(day, 'P'),
      isMonday: isMonday(day),
    }));
  constructor() {}
}
