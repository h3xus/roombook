import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeduration',
  standalone: true
})
export class TimedurationPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if(value.minutes){
      return value.hours+'g.'+value.minutes+'min.'
    } else if (value.hours && !value.minutes) {
      return value.hours+'g.'
    } else {
      return value.minutes+'min'
    }
  }
}
