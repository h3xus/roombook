import { Pipe, PipeTransform } from '@angular/core';
import {format} from 'date-fns';

@Pipe({
  name: 'datePlaceholder',
  standalone: true
})
export class DatePlaceholderPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    return format(value,"yyyy-MM-dd")+'T00:00';
  }

}
