import { Pipe, PipeTransform } from '@angular/core';
import { format } from 'date-fns';


@Pipe({
  name: 'timefmt',
  standalone: true
})
export class TimefmtPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {

    return format(value, 'HH:mm')

  }
}
