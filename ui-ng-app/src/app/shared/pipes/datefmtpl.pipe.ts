import { Pipe, PipeTransform } from '@angular/core';
import { format } from 'date-fns';
import { pl } from 'date-fns/locale';
format(new Date(), 'yyyy-MM-dd')

@Pipe({
  name: 'datefmtpl',
  standalone: true
})
export class DatefmtplPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return format(value, 'dd MMMM (EEEE)', { locale: pl })
  }

}
