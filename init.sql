-- Adminer 4.8.1 MySQL 11.2.2-MariaDB-1:11.2.2+maria~ubu2204 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `bookings` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `bookings`;

DROP TABLE IF EXISTS `booked_rooms`;
CREATE TABLE `booked_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_meeting` varchar(255) DEFAULT NULL,
  `participants` int(22) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `room_id` int(11) NOT NULL DEFAULT 0,
  `status` varchar(255) NOT NULL DEFAULT '',
  `booking_date_start` datetime NOT NULL,
  `booking_date_end` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `booking_params` int(11) NOT NULL DEFAULT 0,
  `optionals` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_user_id` (`user_id`),
  KEY `fk_room_id` (`room_id`),
  KEY `fk_status` (`status`),
  KEY `fk_booking_params` (`booking_params`),
  KEY `fk_optionals` (`optionals`),
  CONSTRAINT `fk_booking_params` FOREIGN KEY (`booking_params`) REFERENCES `room_parameters` (`id`),
  CONSTRAINT `fk_optionals` FOREIGN KEY (`optionals`) REFERENCES `optional_equipment` (`id`),
  CONSTRAINT `fk_room_id` FOREIGN KEY (`room_id`) REFERENCES `conference_room` (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `booked_rooms` (`id`, `title_meeting`, `participants`, `user_id`, `room_id`, `status`, `booking_date_start`, `booking_date_end`, `created_at`, `booking_params`, `optionals`) VALUES
(1, 'Spotkanie zespołu', 10, 1, 7, '0', '2024-02-01 14:00:00', '2024-02-01 15:30:00', '2023-12-11 13:34:00', 1, 1),
(2, 'Spotkanie zarządu', 5, 1, 8, '0', '2024-02-02 09:00:00', '2024-02-02 11:00:00', '2023-12-11 13:35:00', 2, 1),
(3, 'Szkolenie', 20, 1, 9, '0', '2024-02-03 13:00:00', '2024-02-03 16:00:00', '2023-12-11 13:36:00', 3, 1),
(4, 'Dyskusja nad projektem', 8, 1, 10, '0', '2024-02-04 10:30:00', '2024-02-04 12:00:00', '2023-12-11 13:37:00', 4, 1),
(5, 'Spotkanie z klientem', 15, 1, 11, '0', '2024-02-05 15:00:00', '2024-02-05 17:00:00', '2023-12-11 13:38:00', 5, 1),
(6, 'Budowanie zespołu', 30, 61, 12, '0', '2024-02-06 10:00:00', '2024-02-06 15:00:00', '2023-12-11 13:39:00', 6, 1),
(7, 'Wprowadzenie produktu', 50, 1, 13, '0', '2024-02-07 14:00:00', '2024-02-07 17:30:00', '2023-12-11 13:40:00', 7, 1),
(8, 'Rozmowy kwalifikacyjne', 5, 1, 14, '0', '2024-02-08 09:30:00', '2024-02-08 11:30:00', '2023-12-11 13:41:00', 8, 1),
(9, 'Sesja planowania', 12, 1, 15, '0', '2024-02-09 13:00:00', '2024-02-09 15:00:00', '2023-12-11 13:42:00', 9, 1),
(10, 'Warsztaty', 25, 1, 16, '0', '2024-02-10 11:00:00', '2024-02-10 14:00:00', '2023-12-11 13:43:00', 10, 1),
(11, 'Rozpoczęcie projektu', 15, 1, 17, '0', '2024-02-11 10:00:00', '2024-02-11 12:00:00', '2023-12-11 13:44:00', 11, 1),
(12, 'Współpraca zespołowa', 8, 1, 18, '0', '2024-02-12 14:30:00', '2024-02-12 16:30:00', '2023-12-11 13:45:00', 12, 1),
(13, 'Prezentacja klienta', 12, 1, 19, '0', '2024-02-13 09:00:00', '2024-02-13 11:00:00', '2023-12-11 13:46:00', 13, 1),
(14, 'Przegląd zespołu', 10, 1, 20, '0', '2024-02-14 13:30:00', '2024-02-14 15:30:00', '2023-12-11 13:47:00', 14, 1),
(15, 'Demo produktu', 20, 1, 21, '0', '2024-02-15 11:00:00', '2024-02-15 13:00:00', '2023-12-11 13:48:00', 15, 1),
(16, 'Aktualizacja projektu', 7, 1, 22, '0', '2024-02-16 15:00:00', '2024-02-16 16:30:00', '2023-12-11 13:49:00', 16, 1),
(17, 'Warsztaty szkoleniowe', 25, 1, 23, '0', '2024-02-17 10:30:00', '2024-02-17 13:30:00', '2023-12-11 13:50:00', 17, 1),
(18, 'Planowanie projektu', 12, 1, 24, '0', '2024-02-18 09:00:00', '2024-02-18 11:00:00', '2023-12-11 13:51:00', 18, 1),
(19, 'Strategia produktu', 15, 1, 25, '0', '2024-02-19 13:00:00', '2024-02-19 15:00:00', '2023-12-11 13:52:00', 19, 1),
(20, 'Spotkanie z klientem', 18, 1, 26, '0', '2024-02-20 12:30:00', '2024-02-20 14:00:00', '2023-12-11 13:53:00', 20, 1),
(21, 'Warsztaty dla klienta', 15, 1, 27, '0', '2024-03-01 14:00:00', '2024-03-01 16:30:00', '2023-12-11 13:54:00', 21, 1),
(22, 'Huddle zespołu', 10, 1, 28, '0', '2024-03-02 09:30:00', '2024-03-02 10:30:00', '2023-12-11 13:55:00', 22, 1),
(23, 'Spotkanie przedpremierowe produktu', 25, 1, 29, '0', '2024-03-03 11:00:00', '2024-03-03 13:00:00', '2023-12-11 13:56:00', 23, 1),
(24, 'Szkolenie', 15, 1, 30, '0', '2024-03-04 13:30:00', '2024-03-04 15:30:00', '2023-12-11 13:57:00', 24, 1),
(25, 'Demo klienta', 8, 1, 31, '0', '2024-03-05 15:00:00', '2024-03-05 16:30:00', '2023-12-11 13:58:00', 25, 1),
(26, 'Wydarzenie integracyjne zespołu', 30, 1, 32, '0', '2024-03-06 10:00:00', '2024-03-06 15:00:00', '2023-12-11 13:59:00', 26, 1),
(27, 'Dyskusja projektu', 12, 1, 33, '0', '2024-03-07 14:30:00', '2024-03-07 16:30:00', '2023-12-11 14:00:00', 27, 1),
(28, 'Spotkanie zarządu', 20, 1, 34, '0', '2024-03-08 09:00:00', '2024-03-08 11:00:00', '2023-12-11 14:01:00', 28, 1),
(29, 'Sesja strategii produktu', 15, 1, 35, '0', '2024-03-09 13:00:00', '2024-03-09 15:00:00', '2023-12-11 14:02:00', 29, 1),
(30, 'Przegląd zespołu', 10, 1, 36, '0', '2024-03-10 11:30:00', '2024-03-10 13:30:00', '2023-12-11 14:03:00', 30, 1);

DROP TABLE IF EXISTS `booking_status`;
CREATE TABLE `booking_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `booking_status` (`id`, `name`) VALUES
(0,	'confirmed'),
(1,	'normal'),
(2,	'important');

DROP TABLE IF EXISTS `conference_room`;
CREATE TABLE `conference_room` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(255) NOT NULL DEFAULT '',
  `room_description` text NOT NULL,
  `delegation` tinyint(1) NOT NULL DEFAULT 0,
  `localization` tinyint(4) DEFAULT NULL,
  `room_priority` int(11) NOT NULL DEFAULT 0,
  `permanent_equipment` int(11) NOT NULL DEFAULT 0,
  `max_capacity` int(11) NOT NULL,
  `availability` int(11) NOT NULL,
  `session` int(11) NOT NULL,
  `catering` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL DEFAULT 0,
  `updated` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `conference_room` (`room_id`, `name`, `room_description`, `delegation`, `localization`, `room_priority`, `permanent_equipment`, `max_capacity`, `availability`, `session`, `catering`, `created`, `created_by`, `updated`, `updated_by`) VALUES
(1,	'S1',	'Sala sesyjna 1',	0,	10,	0,	1,	18,	0,	0,	1,	'2023-12-11 13:27:24',	0,	'2023-12-11 13:27:24',	0),
(2,	'S2',	'S2',	0,	10,	0,	2,	10,	0,	0,	1,	'2023-12-11 13:27:42',	0,	'2023-12-11 13:27:42',	0),
(3,	'S3',	'S3',	0,	10,	0,	3,	20,	0,	0,	1,	'2023-12-11 13:28:00',	0,	'2023-12-11 13:28:00',	0),
(4,	'BIAŁA',	'Sala Biała',	0,	11,	0,	4,	0,	0,	0,	1,	'2023-12-11 13:28:24',	0,	'2023-12-11 13:28:24',	0),
(5,	'BŁĘKITNA',	'Sala Błękitna',	0,	11,	0,	5,	0,	0,	0,	1,	'2023-12-11 13:28:35',	0,	'2023-12-11 13:28:35',	0),
(6,	'MALINOWA',	'Sala Malinowa',	0,	11,	0,	6,	0,	0,	0,	1,	'2023-12-11 13:28:48',	1,	'2023-12-11 13:28:48',	0),
(7,	'Sala 317',	'',	0,	17,	0,	15,	30,	0,	0,	2,	'2023-12-11 13:29:00',	0,	'2023-12-11 13:29:00',	0),
(8,	'POK 10',	'',	0,	10,	0,	8,	40,	0,	0,	1,	'2023-12-11 13:29:15',	0,	'2023-12-11 13:29:15',	0),
(9,	'POK 5',	'',	0,	10,	0,	0,	10,	0,	0,	1,	'2023-12-11 13:29:30',	0,	'2023-12-11 13:29:30',	0),
(10,	'POK 4',	'',	0,	10,	0,	0,	25,	0,	0,	1,	'2023-12-11 13:29:45',	0,	'2023-12-11 13:29:45',	0),
(11,	'POK 29',	'',	0,	10,	0,	8,	22,	0,	0,	1,	'2023-12-11 13:30:00',	0,	'2023-12-11 13:30:00',	0),
(12,	'POK 267',	'',	0,	12,	0,	9,	10,	0,	0,	0,	'2023-12-11 13:30:15',	0,	'2023-12-11 13:30:15',	0),
(13,	'POK 267a',	'',	0,	12,	0,	0,	8,	0,	0,	1,	'2023-12-11 13:30:30',	0,	'2023-12-11 13:30:30',	0),
(14,	'POK 309',	'',	0,	13,	0,	0,	20,	0,	0,	1,	'2023-12-11 13:30:45',	0,	'2023-12-11 13:30:45',	0),
(15,	'POK 429',	'',	0,	14,	0,	0,	10,	0,	0,	1,	'2023-12-11 13:31:00',	0,	'2023-12-11 13:31:00',	0),
(16,	'BUD. C',	'',	0,	10,	0,	0,	48,	0,	0,	1,	'2023-12-11 13:31:15',	0,	'2023-12-11 13:31:15',	0),
(17,	'Sala 105',	'',	0,	15,	0,	0,	25,	0,	0,	2,	'2023-12-11 13:31:30',	0,	'2023-12-11 13:31:30',	0),
(18,	'Sala 207',	'',	0,	16,	0,	0,	20,	0,	0,	2,	'2023-12-11 13:31:45',	0,	'2023-12-11 13:31:45',	0),
(19,	'Sala 318',	'',	0,	18,	0,	16,	15,	0,	0,	2,	'2023-12-11 13:32:00',	0,	'2023-12-11 13:32:00',	0),
(20,	'Sala 319',	'',	0,	19,	0,	20,	20,	0,	0,	2,	'2023-12-11 13:32:15',	0,	'2023-12-11 13:32:15',	0),
(21,	'Sala 7',	'',	0,	20,	0,	0,	0,	0,	0,	2,	'2023-12-11 13:32:30',	0,	'2023-12-11 13:32:30',	0),
(22,	'Sala 1/21',	'',	0,	21,	0,	17,	0,	0,	0,	2,	'2023-12-11 13:32:45',	0,	'2023-12-11 13:32:45',	0),
(23,	'Sala 2/09',	'',	0,	22,	0,	18,	0,	0,	0,	2,	'2023-12-11 13:33:00',	0,	'2023-12-11 13:33:00',	0),
(24,	'Sala +1',	'',	0,	12,	0,	19,	0,	0,	0,	2,	'2023-12-11 13:33:15',	0,	'2023-12-11 13:33:15',	0),
(25,	'Salka Waga Miejska',	'',	0,	31,	0,	0,	0,	0,	0,	0,	'2023-12-11 13:33:30',	0,	'2023-12-11 13:33:30',	0);

DROP TABLE IF EXISTS `cr_localization`;
CREATE TABLE `cr_localization` (
  `loc_id` int(11) NOT NULL,
  `building` int(11) NOT NULL,
  `floor` int(11) NOT NULL DEFAULT 0,
  `name` char(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`loc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `cr_localization` (`loc_id`, `building`, `floor`, `name`) VALUES
(10,	1,	0,	'Plac Kolegiacki 17 - parter'),
(11,	1,	1,	'Plac Kolegiacki 17 - 1 piętro'),
(12,	1,	2,	'Plac Kolegiacki 17 - 2 piętro'),
(13,	1,	3,	'Plac Kolegiacki 17 - 3 piętro'),
(14,	1,	4,	'Plac Kolegiacki 17 - 4 piętro'),
(15,	2,	1,	'3 Maja 46 - 1 piętro'),
(16,	2,	2,	'3 Maja 46 - 2 piętro'),
(17,	2,	3,	'3 Maja 46 - 3 piętro'),
(18,	3,	3,	'Gronowa 20 - 3 piętro'),
(19,	4,	3,	'Libelta 16/20 - 3 piętro'),
(20,	5,	0,	'Matejki 50 - parter'),
(21,	6,	1,	'Za Bramką 1 - 1 piętro'),
(22,	6,	2,	'Za Bramką 1 - 2 piętro'),
(50,	25,	0,	'Plus +');

DROP TABLE IF EXISTS `optional_equipment`;
CREATE TABLE `optional_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `laptop` tinyint(1) NOT NULL DEFAULT 0,
  `projection_screen` tinyint(1) NOT NULL DEFAULT 0,
  `remote_control` tinyint(1) NOT NULL DEFAULT 0,
  `projector` tinyint(1) NOT NULL DEFAULT 0,
  `battery_sound_system` tinyint(1) NOT NULL DEFAULT 0,
  `computers_speakers` tinyint(1) NOT NULL DEFAULT 0,
  `usb_microphone` tinyint(1) NOT NULL DEFAULT 0,
  `dictaphone` tinyint(1) NOT NULL DEFAULT 0,
  `flipchart` tinyint(1) NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL DEFAULT 0,
  `updated` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `optional_equipment` (`id`, `laptop`, `projection_screen`, `remote_control`, `projector`, `battery_sound_system`, `computers_speakers`, `usb_microphone`, `dictaphone`, `flipchart`, `created`, `created_by`, `updated`, `updated_by`) VALUES
(1,	1,	1,	1,	1,	1,	1,	0,	0,	1,	'2023-12-12 12:32:52',	0,	'2023-12-12 12:32:52',	0),
(2,	0,	0,	0,	0,	0,	0,	0,	0,	0,	'2023-12-12 12:33:01',	0,	'2023-12-12 12:33:01',	0);

DROP TABLE IF EXISTS `permanent_equipment`;
CREATE TABLE `permanent_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `screen` tinyint(1) NOT NULL DEFAULT 0,
  `projector` tinyint(1) NOT NULL DEFAULT 0,
  `numbers_of_microphones` tinyint(1) NOT NULL DEFAULT 0,
  `microphone_stand` tinyint(1) NOT NULL DEFAULT 0,
  `sound_connection` tinyint(1) NOT NULL DEFAULT 0,
  `rostrum` tinyint(1) NOT NULL DEFAULT 0,
  `internet_wifi` tinyint(1) NOT NULL DEFAULT 0,
  `internet_eth` tinyint(1) NOT NULL DEFAULT 0,
  `miejska` tinyint(1) NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL DEFAULT 0,
  `updated` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`room_id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `permanent_equipment` (`id`, `room_id`, `screen`, `projector`, `numbers_of_microphones`, `microphone_stand`, `sound_connection`, `rostrum`, `internet_wifi`, `internet_eth`, `miejska`, `created`, `created_by`, `updated`, `updated_by`) VALUES
(1,	1,	0,	0,	0,	0,	0,	0,	1,	1,	1,	'2024-02-11 08:10:41',	0,	'2024-02-11 08:10:41',	0),
(2,	2,	1,	1,	2,	3,	1,	1,	1,	1,	1,	'2024-02-11 08:12:06',	0,	'2024-02-11 08:12:06',	0),
(3,	3,	1,	1,	2,	0,	1,	0,	1,	1,	1,	'2024-02-11 08:48:57',	0,	'2024-02-11 08:48:57',	0),
(4,	4,	1,	1,	4,	0,	1,	1,	1,	1,	1,	'2024-02-11 08:50:35',	0,	'2024-02-11 08:50:35',	0),
(5,	5,	1,	1,	2,	0,	1,	0,	1,	1,	1,	'2024-02-11 08:51:53',	0,	'2024-02-11 08:51:53',	0),
(6,	6,	1,	1,	2,	0,	0,	0,	1,	1,	1,	'2024-02-11 08:52:41',	0,	'2024-02-11 08:52:41',	0),
(15,	7,	1,	1,	0,	0,	0,	0,	0,	1,	1,	'2024-02-11 11:39:08',	0,	'2024-02-11 11:39:08',	0),
(7,	8,	1,	1,	0,	0,	0,	0,	1,	1,	1,	'2024-02-11 08:54:18',	0,	'2024-02-11 08:54:18',	0),
(8,	11,	1,	1,	0,	0,	0,	0,	1,	1,	1,	'2024-02-11 10:30:07',	0,	'2024-02-11 10:30:07',	0),
(9,	12,	1,	1,	0,	0,	0,	0,	0,	1,	1,	'2024-02-11 11:22:30',	0,	'2024-02-11 11:22:30',	0),
(10,	14,	1,	1,	0,	0,	0,	0,	1,	1,	1,	'2024-02-11 11:25:31',	0,	'2024-02-11 11:25:31',	0),
(11,	15,	1,	1,	0,	0,	0,	0,	1,	1,	1,	'2024-02-11 11:26:18',	0,	'2024-02-11 11:26:18',	0),
(12,	16,	1,	1,	0,	0,	0,	0,	0,	1,	1,	'2024-02-11 11:26:54',	0,	'2024-02-11 11:26:54',	0),
(13,	17,	1,	1,	0,	0,	0,	0,	0,	1,	1,	'2024-02-11 11:27:31',	0,	'2024-02-11 11:27:31',	0),
(14,	18,	1,	1,	0,	0,	0,	0,	0,	1,	1,	'2024-02-11 11:28:01',	0,	'2024-02-11 11:28:01',	0),
(16,	19,	1,	1,	0,	0,	0,	0,	0,	1,	1,	'2024-02-11 11:43:14',	0,	'2024-02-11 11:43:14',	0),
(20,	20,	1,	1,	0,	0,	0,	0,	0,	0,	0,	'2024-02-11 12:50:59',	0,	'2024-02-11 12:50:59',	0),
(17,	22,	1,	1,	0,	0,	0,	0,	0,	0,	0,	'2024-02-11 12:44:12',	0,	'2024-02-11 12:44:12',	0),
(18,	23,	1,	1,	0,	0,	0,	0,	0,	1,	1,	'2024-02-11 12:44:47',	0,	'2024-02-11 12:44:47',	0),
(19,	24,	1,	1,	0,	0,	0,	0,	1,	0,	0,	'2024-02-11 12:45:26',	0,	'2024-02-11 12:45:26',	0);

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL DEFAULT '',
  `created` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `roles` (`id`, `name`, `created`) VALUES
(100,	'Normal',	0),
(500,	'admin',	0);

DROP TABLE IF EXISTS `room_parameters`;
CREATE TABLE `room_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maximum_capacility` int(11) NOT NULL DEFAULT 0,
  `availability` int(11) NOT NULL DEFAULT 0,
  `session` tinyint(1) NOT NULL DEFAULT 0,
  `setting` int(11) NOT NULL DEFAULT 0,
  `tables` int(11) NOT NULL DEFAULT 0,
  `cathering` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL DEFAULT 0,
  `updated` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `availability` (`availability`),
  KEY `setting` (`setting`),
  KEY `tables` (`tables`),
  KEY `cathering` (`cathering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `room_parameters` (`id`, `maximum_capacility`, `availability`, `session`, `setting`, `tables`, `cathering`, `created`, `created_by`, `updated`, `updated_by`) VALUES
(1,	0,	0,	1,	1,	0,	NULL,	'2023-12-12 07:37:42',	0,	'2023-12-12 07:37:42',	0);

DROP TABLE IF EXISTS `rp_availability`;
CREATE TABLE `rp_availability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `rp_availability` (`id`, `name`) VALUES
(0,	'Sala dostępna bez ograniczeń'),
(1,	'Dotyczy [POK 1, 6, 7] (pon - piąt 7.30 - 12.00) (13.00 za zgodą Biura RM)');

DROP TABLE IF EXISTS `rp_catering`;
CREATE TABLE `rp_catering` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `rp_catering` (`id`, `name`) VALUES
(0,	'Brak cateringu'),
(1,	'TAK (z obsługą)'),
(2,	'TAK (własny transport)');

DROP TABLE IF EXISTS `rp_settings`;
CREATE TABLE `rp_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `rp_settings` (`id`, `name`) VALUES
(1,	'Tradycyjne'),
(2,	'Koncertowe');

DROP TABLE IF EXISTS `rp_tables`;
CREATE TABLE `rp_tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `rp_tables` (`id`, `name`) VALUES
(1,	'Przy stołac'),
(2,	'Wokół stołu'),
(3,	'Brak stołów');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL DEFAULT '',
  `email` char(100) NOT NULL,
  `role` int(11) NOT NULL DEFAULT 0,
  `isSuspended` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `role` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `users` (`id`, `name`, `email`, `role`, `isSuspended`) VALUES
(1,	'barceg',	'barceg@um.poznan.pl',	1,	0);

-- 2024-02-11 13:09:59